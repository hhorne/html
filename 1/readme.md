# HTML Intro

The goal here will be to take the basic HTML document provided and add onto it to turn it into a helpful page of notes for you.

To help you in understanding HTML, you can use the following link for reference: [Mozilla Developer Network: Getting Started with HTML](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started)

![Example of completed exercise](example-complete.png)